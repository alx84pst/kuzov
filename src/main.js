import '@babel/polyfill'
import Vue from 'vue'
import './plugins/vuetify'
import store from './store/store.js'
import App from './App.vue'
import 'vuetify/src/stylus/app.styl'

Vue.config.productionTip = false

new Vue({
    store,
    render: h => h(App)
}).$mount('#app')